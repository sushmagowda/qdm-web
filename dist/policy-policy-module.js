(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["policy-policy-module"],{

/***/ "./src/app/policy/policy-routing.module.ts":
/*!*************************************************!*\
  !*** ./src/app/policy/policy-routing.module.ts ***!
  \*************************************************/
/*! exports provided: PolicyRoutingModule, routedComponents */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PolicyRoutingModule", function() { return PolicyRoutingModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routedComponents", function() { return routedComponents; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _policy_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./policy.component */ "./src/app/policy/policy.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: _policy_component__WEBPACK_IMPORTED_MODULE_2__["PolicyComponent"],
        data: {
            title: 'DiviceList'
        },
    }
];
var PolicyRoutingModule = /** @class */ (function () {
    function PolicyRoutingModule() {
    }
    PolicyRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
        })
    ], PolicyRoutingModule);
    return PolicyRoutingModule;
}());

var routedComponents = [_policy_component__WEBPACK_IMPORTED_MODULE_2__["PolicyComponent"]];


/***/ }),

/***/ "./src/app/policy/policy.component.html":
/*!**********************************************!*\
  !*** ./src/app/policy/policy.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Filter Datatable Options Starts -->\n<section id=\"filter\" class=\"mb-3\">\n    <div class=\"row text-left\">\n        <div class=\"col-12\">\n            <div class=\"content-header\">Policy </div>\n        </div>\n    </div>\n    <div class=\"row\">\n        <div class=\"col-sm-12\">\n            <input type='text' style='padding:8px;margin:15px auto;width:30%;' placeholder='Type to filter '\n            />\n            <!-- <button type='text' style='padding:8px;margin:20px auto;width:15%;margin-left: 1030px;'>Add Policy </button> -->\n            <button type='text'class=\"btn btn-social btn-min-width mr-2 mb-2 btn-reddit\" (click)=\"open(content)\" style='margin-left: 650px;'><span class=\"ft ft-plus\"></span>Add Policy </button>\n\n            <!-- <button type='text' style='padding:8px;margin:20px auto;width:15%;margin-left: 520px;'>Add  </button> -->\n            <ngx-datatable #table class='bootstrap' [columns]=\"columns\" [columnMode]=\"'force'\" [headerHeight]=\"50\" [footerHeight]=\"50\"\n                [rowHeight]=\"'auto'\" [limit]=\"10\" [rows]='rows' [selected]=\"selected\" [selectionType]=\"'cell'\" (select)=\"onSelect($event)\">\n            </ngx-datatable>\n        </div>\n\n        <!-- Button trigger modal -->\n\n    </div>\n</section>\n<!-- Filter Datatable Options Ends -->\n\n        <div class=\"card-body\">\n            <div class=\"card-block\">\n                <ng-template #content let-c=\"close\" let-d=\"dismiss\">\n                    <div class=\"modal-header\">\n                        <h4 class=\"modal-title\">Add Policy</h4>\n                        <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"d('Cross click')\">\n                        <span aria-hidden=\"true\">&times;</span>\n                        </button>\n                    </div>\n                    <div class=\"modal-body\">\n\n\n                      \t<div class=\"row text-left justify-content-md-center\">\n                      \t\t<div class=\"\">\n                      \t\t\t<div class=\"\" style=\"width:400px\">\n                      \t\t\t\t<div class=\"card-body\">\n                      \t\t\t\t\t<div class=\"px-3\">\n                      \t\t\t\t\t\t<form class=\"form\">\n                      \t\t\t\t\t\t\t<div class=\"form-body\">\n\n                      \t\t\t\t\t\t\t\t<div class=\"form-group\">\n                      \t\t\t\t\t\t\t\t\t<label for=\"eventRegInput1\">Policy Name</label>\n                      \t\t\t\t\t\t\t\t\t<input type=\"text\" id=\"eventRegInput1\" class=\"form-control\"  name=\"fullname\">\n                      \t\t\t\t\t\t\t\t</div>\n\n                      \t\t\t\t\t\t\t\t<div class=\"form-group\">\n                      \t\t\t\t\t\t\t\t\t<label for=\"eventRegInput2\">Policy Description</label>\n                      \t\t\t\t\t\t\t\t\t<input type=\"text\" id=\"eventRegInput2\" class=\"form-control\"  name=\"title\">\n                      \t\t\t\t\t\t\t\t</div>\n\n\n                      \t\t\t\t\t\t\t</div>\n\n                      \t\t\t\t\t\t\t<div class=\"form-actions center\">\n                      \t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-raised btn-warning mr-1\">\n                      \t\t\t\t\t\t\t\t\t<i class=\"ft-x\"></i> Cancel\n                      \t\t\t\t\t\t\t\t</button>\n                      \t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-raised btn-primary\">\n                      \t\t\t\t\t\t\t\t\t<i class=\"fa fa-check-square-o\"></i> Save\n                      \t\t\t\t\t\t\t\t</button>\n                      \t\t\t\t\t\t\t</div>\n                      \t\t\t\t\t\t</form>\n                      \t\t\t\t\t</div>\n                      \t\t\t\t</div>\n                      \t\t\t</div>\n                      \t\t</div>\n                      \t</div>\n\n\n                    </div>\n                    <div class=\"modal-footer\">\n                        <button type=\"button\" class=\"btn btn-secondary btn-raised\" (click)=\"c('Close click')\">Close</button>\n                    </div>\n                </ng-template>\n\n            </div>\n        </div>\n"

/***/ }),

/***/ "./src/app/policy/policy.component.scss":
/*!**********************************************!*\
  !*** ./src/app/policy/policy.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/policy/policy.component.ts":
/*!********************************************!*\
  !*** ./src/app/policy/policy.component.ts ***!
  \********************************************/
/*! exports provided: PolicyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PolicyComponent", function() { return PolicyComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _swimlane_ngx_datatable_release__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @swimlane/ngx-datatable/release */ "./node_modules/@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var _swimlane_ngx_datatable_release__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_swimlane_ngx_datatable_release__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var data = __webpack_require__(/*! ../shared/data/policy.json */ "./src/app/shared/data/policy.json");
var PolicyComponent = /** @class */ (function () {
    function PolicyComponent(router, route, modalService) {
        this.router = router;
        this.route = route;
        this.modalService = modalService;
        this.rows = [];
        this.selected = [];
        this.temp = [];
        // Table Column Titles
        this.columns = [
            { prop: 'policy' },
            { name: 'Groups' },
            { name: 'Devices' }
        ];
        this.temp = data.slice();
        this.rows = data;
    }
    PolicyComponent.prototype.onSelect = function (event) {
        console.log("onslect");
        this.router.navigate(['/taskboard-ngrx'], { relativeTo: this.route.parent });
    };
    //  On Activation of dataTable's data row
    PolicyComponent.prototype.onActivate = function (event) {
        console.log("onaCTIVATE");
    };
    // Open default modal
    PolicyComponent.prototype.open = function (content) {
        var _this = this;
        this.modalService.open(content).result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
        });
    };
    // This function is used in open
    PolicyComponent.prototype.getDismissReason = function (reason) {
        if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["ModalDismissReasons"].ESC) {
            return 'by pressing ESC';
        }
        else if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["ModalDismissReasons"].BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        }
        else {
            return "with: " + reason;
        }
    };
    // Open modal with dark section
    PolicyComponent.prototype.openModal = function (customContent) {
        this.modalService.open(customContent, { windowClass: 'dark-modal' });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_swimlane_ngx_datatable_release__WEBPACK_IMPORTED_MODULE_1__["DatatableComponent"]),
        __metadata("design:type", _swimlane_ngx_datatable_release__WEBPACK_IMPORTED_MODULE_1__["DatatableComponent"])
    ], PolicyComponent.prototype, "table", void 0);
    PolicyComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-policy',
            template: __webpack_require__(/*! ./policy.component.html */ "./src/app/policy/policy.component.html"),
            styles: [__webpack_require__(/*! ./policy.component.scss */ "./src/app/policy/policy.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModal"]])
    ], PolicyComponent);
    return PolicyComponent;
}());



/***/ }),

/***/ "./src/app/policy/policy.module.ts":
/*!*****************************************!*\
  !*** ./src/app/policy/policy.module.ts ***!
  \*****************************************/
/*! exports provided: PolicyModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PolicyModule", function() { return PolicyModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _policy_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./policy-routing.module */ "./src/app/policy/policy-routing.module.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var ngx_quill__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-quill */ "./node_modules/ngx-quill/index.js");
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @swimlane/ngx-datatable */ "./node_modules/@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _policy_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./policy.component */ "./src/app/policy/policy.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var PolicyModule = /** @class */ (function () {
    function PolicyModule() {
    }
    PolicyModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _policy_routing_module__WEBPACK_IMPORTED_MODULE_2__["PolicyRoutingModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModule"],
                ngx_quill__WEBPACK_IMPORTED_MODULE_4__["QuillModule"],
                _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_5__["NgxDatatableModule"]
            ],
            declarations: [
                _policy_component__WEBPACK_IMPORTED_MODULE_6__["PolicyComponent"]
            ]
        })
    ], PolicyModule);
    return PolicyModule;
}());



/***/ }),

/***/ "./src/app/shared/data/policy.json":
/*!*****************************************!*\
  !*** ./src/app/shared/data/policy.json ***!
  \*****************************************/
/*! exports provided: 0, default */
/***/ (function(module) {

module.exports = [{"policy":"default","groups":"1","devices":"8"}];

/***/ })

}]);
//# sourceMappingURL=policy-policy-module.js.map