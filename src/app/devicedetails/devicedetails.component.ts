import { Component, ViewChild,OnInit} from '@angular/core';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";
import { Router, ActivatedRoute } from "@angular/router";
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

declare var require: any;
const data: any = require('../shared/data/policy.json');

@Component({
  selector: 'app-devicedetails',
  templateUrl: './devicedetails.component.html',
  styleUrls: ['./devicedetails.component.scss']
})
export class DevicedetailsComponent implements OnInit {
  public map: any = { lat: 12.9716, lng: 77.5946 };
  loseResult: string;
    rows = [];
  selected: any[] = [];
    temp = [];

    // Table Column Titles
    columns = [
        { name: 'App Name' },
        { name: 'Version' },
        { name: 'Created Date' },
        { name: 'Data Usage(in MB)' },
        { name: 'Mobile Usage(in MB)' },
        { name: 'Wifi Usage(in MB)' },
        { name: 'Status' }
    ];
  constructor() { }

  ngOnInit() {
  }

}
