import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { PolicydetailsRoutingModule } from "./policydetails-routing.module";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { QuillModule } from 'ngx-quill'
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { PolicydetailsComponent } from "./policydetails.component";


@NgModule({
    imports: [
        CommonModule,
        PolicydetailsRoutingModule,
        NgbModule,
        QuillModule,
        NgxDatatableModule
    ],
    declarations: [
        PolicydetailsComponent
    ]
})
export class PolicydetailsModule { }
