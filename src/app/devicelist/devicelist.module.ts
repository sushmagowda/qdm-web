import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { DeviceListRoutingModule } from "./devicelist-routing.module";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { QuillModule } from 'ngx-quill'
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { DevicelistComponent } from "./devicelist.component";


@NgModule({
    imports: [
        CommonModule,
        DeviceListRoutingModule,
        NgbModule,
        QuillModule,
        NgxDatatableModule
    ],
    declarations: [
        DevicelistComponent
    ]
})
export class DevicelistModule { }
