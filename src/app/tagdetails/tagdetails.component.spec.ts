import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TagdetailsComponent } from './tagdetails.component';

describe('TagdetailsComponent', () => {
  let component: TagdetailsComponent;
  let fixture: ComponentFixture<TagdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TagdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
