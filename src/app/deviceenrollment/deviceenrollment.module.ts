import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { DeviceEnrollmentRoutingModule } from "./deviceenrollment-routing.module";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { QuillModule } from 'ngx-quill'
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { DeviceenrollmentComponent } from "./deviceenrollment.component";


@NgModule({
    imports: [
        CommonModule,
        DeviceEnrollmentRoutingModule,
        NgbModule,
        QuillModule,
        NgxDatatableModule
    ],
    declarations: [
        DeviceenrollmentComponent
    ]
})
export class DeviceenrollmentModule { }
