import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DeviceenrollmentComponent } from './deviceenrollment.component';

const routes: Routes = [
  {
    path: '',
     component: DeviceenrollmentComponent,
    data: {
      title: 'DivicEnrollment'
    },

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DeviceEnrollmentRoutingModule { }

export const routedComponents = [DeviceenrollmentComponent];
