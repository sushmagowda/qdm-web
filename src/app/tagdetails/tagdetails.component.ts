import { Component,ViewChild} from '@angular/core';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";
import { Router, ActivatedRoute } from "@angular/router";
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';


declare var require: any;
const data: any = require('../shared/data/device.json');

@Component({
  selector: 'app-tagdetails',
  templateUrl: './tagdetails.component.html',
  styleUrls: ['./tagdetails.component.scss']
})
export class TagdetailsComponent {

  rows = [];
  closeResult: string;
  selected: any[] = [];
  temp = [];
  preapproveddevice = 'true';
  licencekey = 'false';

  // Table Column Titles
  columns = [
      { name: 'IMEI Number' },
      { name: 'Mobile Number' },
      { name: 'User Name' },
      { name: 'Status' },
      { name: 'Action' }
  ];

  columnslicenced = [
    { name: 'First Name' },
    { name: 'Last Name' },
    { name: 'Action' }
  ]
  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(private router: Router,
      private route: ActivatedRoute,private modalService: NgbModal ) {
      this.temp = [...data];
      this.rows = data;
  }

  enablepreapproved(event:any){
    if(event.activeId=='ngb-tab-4')
    {
    this.preapproveddevice = 'true' ;
    this.licencekey = 'false';
    console.log(event)
    }
    else{
      this.preapproveddevice = 'false' ;
      this.licencekey = 'true';
    }
  }


  onSelect(event) {
   console.log("onslect");
     this.router.navigate(['/taskboard'], { relativeTo: this.route.parent });
  }

  //  On Activation of dataTable's data row
  onActivate(event) {
    console.log("onaCTIVATE");
  }
  //   updateFilter(event) {
  //     console.log(event);
  //       const val = event.target.value.toLowerCase();
  //
  //       // filter our data
  //       const temp = this.temp.filter(function (d) {
  //           return d.name.toLowerCase().indexOf(val) !== -1 || !val;
  //       });
  //
  //       // update the rows
  //       this.rows = temp;
  //       // Whenever the filter changes, always go back to the first page
  //       this.table.offset = 0;
  //   // }

}
