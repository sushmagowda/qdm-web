import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { DeviceDetailsRoutingModule } from "./devicedetails-routing.module";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { QuillModule } from 'ngx-quill'
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { DevicedetailsComponent } from "./devicedetails.component";
import { AgmCoreModule } from '@agm/core';

@NgModule({
    imports: [
        CommonModule,
        DeviceDetailsRoutingModule,
        NgbModule,
        QuillModule,
        NgxDatatableModule,
        AgmCoreModule
    ],
    declarations: [
        DevicedetailsComponent
    ]
})
export class DevicedetailsModule { }
