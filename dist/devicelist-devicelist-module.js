(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["devicelist-devicelist-module"],{

/***/ "./src/app/devicelist/devicelist-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/devicelist/devicelist-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: DeviceListRoutingModule, routedComponents */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeviceListRoutingModule", function() { return DeviceListRoutingModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routedComponents", function() { return routedComponents; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _devicelist_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./devicelist.component */ "./src/app/devicelist/devicelist.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: _devicelist_component__WEBPACK_IMPORTED_MODULE_2__["DevicelistComponent"],
        data: {
            title: 'DiviceList'
        },
    }
];
var DeviceListRoutingModule = /** @class */ (function () {
    function DeviceListRoutingModule() {
    }
    DeviceListRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
        })
    ], DeviceListRoutingModule);
    return DeviceListRoutingModule;
}());

var routedComponents = [_devicelist_component__WEBPACK_IMPORTED_MODULE_2__["DevicelistComponent"]];


/***/ }),

/***/ "./src/app/devicelist/devicelist.component.html":
/*!******************************************************!*\
  !*** ./src/app/devicelist/devicelist.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Filter Datatable Options Starts -->\n<section id=\"filter\" class=\"mb-3\">\n    <div class=\"row text-left\">\n        <div class=\"col-12\">\n            <div class=\"content-header\">Device</div>\n        </div>\n    </div>\n    <div class=\"row\">\n        <div class=\"col-sm-12\">\n            <input type='text' style='padding:8px;margin:15px auto;width:30%;' placeholder='Type to filter '\n            />\n            <!-- <button type='text'class=\"btn btn-social btn-min-width mr-2 mb-2 btn-reddit\"  style='margin-left: 650px;'><span class=\"ft ft-plus\"></span>Add Device </button> -->\n\n            <ngx-datatable #table class='bootstrap' [columns]=\"columns\" [columnMode]=\"'force'\" [headerHeight]=\"50\" [footerHeight]=\"50\"\n                [rowHeight]=\"'auto'\" [limit]=\"10\" [rows]='rows' [selected]=\"selected\" [selectionType]=\"'cell'\" (select)=\"onSelect($event)\">\n                <ngx-datatable-column *ngFor=\"let col of columns\" [name]=\"col.name\">\n                </ngx-datatable-column>\n                          <ngx-datatable-column name=\"Action\" sortable=\"false\" >\n                            <ng-template let-row=\"data\" let-value=\"value\" ngx-datatable-cell-template>\n                              <button class=\"btn btn-secondary btn-fab btn-delete\">\n                                  <i  class=\"fa fa-trash\"></i>\n                              </button>\n                            </ng-template>\n              </ngx-datatable-column>\n\n            </ngx-datatable>\n\n\n        </div>\n    </div>\n</section>\n<!-- Filter Datatable Options Ends -->\n"

/***/ }),

/***/ "./src/app/devicelist/devicelist.component.scss":
/*!******************************************************!*\
  !*** ./src/app/devicelist/devicelist.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn-delete {\n  margin-bottom: 0rem !important;\n  padding: 0rem 0.375rem !important; }\n"

/***/ }),

/***/ "./src/app/devicelist/devicelist.component.ts":
/*!****************************************************!*\
  !*** ./src/app/devicelist/devicelist.component.ts ***!
  \****************************************************/
/*! exports provided: DevicelistComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DevicelistComponent", function() { return DevicelistComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _swimlane_ngx_datatable_release__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @swimlane/ngx-datatable/release */ "./node_modules/@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var _swimlane_ngx_datatable_release__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_swimlane_ngx_datatable_release__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var data = __webpack_require__(/*! ../shared/data/device.json */ "./src/app/shared/data/device.json");
var DevicelistComponent = /** @class */ (function () {
    function DevicelistComponent(router, route) {
        this.router = router;
        this.route = route;
        this.rows = [];
        this.selected = [];
        this.temp = [];
        // Table Column Titles
        this.columns = [
            { name: 'ImeiNumber' },
            { name: 'Mobile' },
            { name: 'Manufacturer' },
            { name: 'Model' },
            { name: 'Date' },
            { name: 'Status' }
        ];
        this.temp = data.slice();
        this.rows = data;
    }
    DevicelistComponent.prototype.onSelect = function (event) {
        console.log("onslect");
        this.router.navigate(['/taskboard'], { relativeTo: this.route.parent });
    };
    //  On Activation of dataTable's data row
    DevicelistComponent.prototype.onActivate = function (event) {
        console.log("onaCTIVATE");
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_swimlane_ngx_datatable_release__WEBPACK_IMPORTED_MODULE_1__["DatatableComponent"]),
        __metadata("design:type", _swimlane_ngx_datatable_release__WEBPACK_IMPORTED_MODULE_1__["DatatableComponent"])
    ], DevicelistComponent.prototype, "table", void 0);
    DevicelistComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-devicelist',
            template: __webpack_require__(/*! ./devicelist.component.html */ "./src/app/devicelist/devicelist.component.html"),
            styles: [__webpack_require__(/*! ./devicelist.component.scss */ "./src/app/devicelist/devicelist.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], DevicelistComponent);
    return DevicelistComponent;
}());



/***/ }),

/***/ "./src/app/devicelist/devicelist.module.ts":
/*!*************************************************!*\
  !*** ./src/app/devicelist/devicelist.module.ts ***!
  \*************************************************/
/*! exports provided: DevicelistModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DevicelistModule", function() { return DevicelistModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _devicelist_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./devicelist-routing.module */ "./src/app/devicelist/devicelist-routing.module.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var ngx_quill__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-quill */ "./node_modules/ngx-quill/index.js");
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @swimlane/ngx-datatable */ "./node_modules/@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _devicelist_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./devicelist.component */ "./src/app/devicelist/devicelist.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var DevicelistModule = /** @class */ (function () {
    function DevicelistModule() {
    }
    DevicelistModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _devicelist_routing_module__WEBPACK_IMPORTED_MODULE_2__["DeviceListRoutingModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModule"],
                ngx_quill__WEBPACK_IMPORTED_MODULE_4__["QuillModule"],
                _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_5__["NgxDatatableModule"]
            ],
            declarations: [
                _devicelist_component__WEBPACK_IMPORTED_MODULE_6__["DevicelistComponent"]
            ]
        })
    ], DevicelistModule);
    return DevicelistModule;
}());



/***/ }),

/***/ "./src/app/shared/data/device.json":
/*!*****************************************!*\
  !*** ./src/app/shared/data/device.json ***!
  \*****************************************/
/*! exports provided: 0, 1, 2, default */
/***/ (function(module) {

module.exports = [{"imeiNumber":"845632211589665","mobile":"8765432156","manufacturer":"vivo","model":"vivo123","date":"2018-05-08 18:01:40","status":"Enrolled"},{"imeiNumber":"845632211589665","mobile":"8765432156","manufacturer":"vivo","model":"vivo123","date":"2018-05-08 18:01:40","status":"Enrolled"},{"imeiNumber":"845632211589665","mobile":"8765432156","manufacturer":"vivo","model":"vivo123","date":"2018-05-08 18:01:40","Created Date":"2018-05-08 18:01:40","status":"Enrolled"}];

/***/ })

}]);
//# sourceMappingURL=devicelist-devicelist-module.js.map