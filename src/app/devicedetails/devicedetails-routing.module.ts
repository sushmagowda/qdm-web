import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DevicedetailsComponent } from './devicedetails.component';

const routes: Routes = [
  {
    path: '',
     component: DevicedetailsComponent,
    data: {
      title: 'DiviceList'
    },

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DeviceDetailsRoutingModule { }

export const routedComponents = [DevicedetailsComponent];
