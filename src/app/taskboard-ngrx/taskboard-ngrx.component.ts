import { Component, ViewEncapsulation, ViewChild, ElementRef, OnInit } from '@angular/core';
import { Task } from './taskboard-ngrx.model';

import { Store } from '@ngrx/store';
import { Observable ,  Subscription } from 'rxjs';
declare var require: any;
const data: any = require('../shared/data/apppolicy.json');
const wifidata: any = require('../shared/data/wifipolicy.json');

@Component({
  selector: 'app-ngrx-taskboard',
  templateUrl: './taskboard-ngrx.component.html',
  styleUrls: ['./taskboard-ngrx.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TaskboardNGRXComponent implements OnInit {

  rows = [];
  syncrows = [];
  wifirows = [];
  brandingrows = [];
  devicerows = [];
  loadingIndicator: boolean = true;
  reorderable: boolean = true;

  // DataTable Content Titles
  columns = [
      { prop: 'name' },
      { name: 'Packagename' },
      { name: 'Type' }
  ];

  wificolumns = [
    { prop: 'name' },
    { name: 'Refreshdate' },
    { name: 'Datalimit' }
  ]

  constructor() {
      this.rows = data;
      this.syncrows = data;
      this.wifirows = wifidata;
      this.brandingrows = data;
      this.devicerows = data;
      console.log(this.rows);
      setTimeout(() => { this.loadingIndicator = false; }, 1500);
  }
  ngOnInit() {
  }


}
