import { Component,ViewChild} from '@angular/core';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";
import { Router, ActivatedRoute } from "@angular/router";
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';


declare var require: any;
const data: any = require('../shared/data/tags.json');

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})
export class PlayerComponent {
  closeResult: string;
  rows = [];
  selected: any[] = [];
  temp = [];

  // Table Column Titles
  columns = [
      { prop: 'Name' },
      { name: 'Policy' },
      { name: 'Department' },
      { name: 'Devicecount' },
      { name: 'Usercount' },
      { name: 'Description' },

  ];
  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(private router: Router,
      private route: ActivatedRoute ,private modalService: NgbModal) {
      this.temp = [...data];
      this.rows = data;
  }

  onSelect(event) {
   console.log("onslect");
     this.router.navigate(['/tagdetails'], { relativeTo: this.route.parent });
  }

  //  On Activation of dataTable's data row
  onActivate(event) {
    console.log("onaCTIVATE");
  }
//   updateFilter(event) {
//     console.log(event);
//       const val = event.target.value.toLowerCase();
//
//       // filter our data
//       const temp = this.temp.filter(function (d) {
//           return d.name.toLowerCase().indexOf(val) !== -1 || !val;
//       });
//
//       // update the rows
//       this.rows = temp;
//       // Whenever the filter changes, always go back to the first page
//       this.table.offset = 0;
//   // }


  // Open default modal
  open(content) {
      this.modalService.open(content).result.then((result) => {
          this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
  }

  // This function is used in open
  private getDismissReason(reason: any): string {
      if (reason === ModalDismissReasons.ESC) {
          return 'by pressing ESC';
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
          return 'by clicking on a backdrop';
      } else {
          return `with: ${reason}`;
      }
  }

  // Open modal with dark section
  openModal(customContent) {
      this.modalService.open(customContent, { windowClass: 'dark-modal' });
  }

 }
