import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeviceenrollmentComponent } from './deviceenrollment.component';

describe('DeviceenrollmentComponent', () => {
  let component: DeviceenrollmentComponent;
  let fixture: ComponentFixture<DeviceenrollmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeviceenrollmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeviceenrollmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
