import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PolicydetailsComponent } from './policydetails.component';

const routes: Routes = [
  {
    path: '',
     component: PolicydetailsComponent,
    data: {
      title: 'policy Details'
    },

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PolicydetailsRoutingModule { }

export const routedComponents = [PolicydetailsComponent];
