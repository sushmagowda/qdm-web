import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DevicelistComponent } from './devicelist.component';

const routes: Routes = [
  {
    path: '',
     component: DevicelistComponent,
    data: {
      title: 'DiviceList'
    },

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DeviceListRoutingModule { }

export const routedComponents = [DevicelistComponent];
