import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { PolicyRoutingModule } from "./policy-routing.module";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { QuillModule } from 'ngx-quill'
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { PolicyComponent } from "./policy.component";


@NgModule({
    imports: [
        CommonModule,
        PolicyRoutingModule,
        NgbModule,
        QuillModule,
        NgxDatatableModule
    ],
    declarations: [
        PolicyComponent
    ]
})
export class PolicyModule { }
