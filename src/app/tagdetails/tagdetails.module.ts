import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { TagDetailsRoutingModule } from "./tagdetails-routing.module";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { QuillModule } from 'ngx-quill'
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { TagdetailsComponent } from "./tagdetails.component";


@NgModule({
    imports: [
        CommonModule,
        TagDetailsRoutingModule,
        NgbModule,
        QuillModule,
        NgxDatatableModule
    ],
    declarations: [
        TagdetailsComponent
    ]
})
export class TagdetailsModule { }
