import { Component,ViewChild} from '@angular/core';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";
import { Router, ActivatedRoute } from "@angular/router";
declare var require: any;
const data: any = require('../shared/data/device.json');

@Component({
  selector: 'app-devicelist',
  templateUrl: './devicelist.component.html',
  styleUrls: ['./devicelist.component.scss']
})
export class DevicelistComponent  {
  rows = [];
selected: any[] = [];
  temp = [];

  // Table Column Titles
  columns = [
      { name: 'ImeiNumber' },
      { name: 'Mobile' },
      { name: 'Manufacturer' },
      { name: 'Model' },
      { name: 'Date' },
      { name: 'Status' }
  ];
  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(private router: Router,
      private route: ActivatedRoute ) {
      this.temp = [...data];
      this.rows = data;
  }

  onSelect(event) {
   console.log("onslect");
     this.router.navigate(['/devicedetails'], { relativeTo: this.route.parent });
  }

  //  On Activation of dataTable's data row
  onActivate(event) {
    console.log("onaCTIVATE");
  }
//   updateFilter(event) {
//     console.log(event);
//       const val = event.target.value.toLowerCase();
//
//       // filter our data
//       const temp = this.temp.filter(function (d) {
//           return d.name.toLowerCase().indexOf(val) !== -1 || !val;
//       });
//
//       // update the rows
//       this.rows = temp;
//       // Whenever the filter changes, always go back to the first page
//       this.table.offset = 0;
//   // }
 }
