import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { TagsRoutingModule } from "./tags-routing.module";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { QuillModule } from 'ngx-quill'
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { TagsComponent } from "./tags.component";


@NgModule({
    imports: [
        CommonModule,
        TagsRoutingModule,
        NgbModule,
        QuillModule,
        NgxDatatableModule
    ],
    declarations: [
        TagsComponent
    ]
})
export class TagsModule { }
