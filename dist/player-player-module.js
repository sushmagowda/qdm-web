(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["player-player-module"],{

/***/ "./src/app/player/player-routing.module.ts":
/*!*************************************************!*\
  !*** ./src/app/player/player-routing.module.ts ***!
  \*************************************************/
/*! exports provided: PlayerRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PlayerRoutingModule", function() { return PlayerRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _player_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./player.component */ "./src/app/player/player.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: _player_component__WEBPACK_IMPORTED_MODULE_2__["PlayerComponent"],
        data: {
            title: 'Player'
        },
    }
];
var PlayerRoutingModule = /** @class */ (function () {
    function PlayerRoutingModule() {
    }
    PlayerRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
        })
    ], PlayerRoutingModule);
    return PlayerRoutingModule;
}());



/***/ }),

/***/ "./src/app/player/player.component.html":
/*!**********************************************!*\
  !*** ./src/app/player/player.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Filter Datatable Options Starts -->\r\n<section id=\"filter\" class=\"mb-3\">\r\n    <div class=\"row text-left\">\r\n        <div class=\"col-12\">\r\n            <div class=\"content-header\">Tags</div>\r\n        </div>\r\n    </div>\r\n    <div class=\"row\">\r\n        <div class=\"col-sm-12\">\r\n            <input type='text' style='padding:8px;margin:15px auto;width:30%;' placeholder='Type to filter '\r\n            />\r\n            <button type='text' class=\"btn btn-social btn-min-width mr-2 mb-2 btn-reddit\" style='margin-left: 650px;' (click)=\"open(content)\"><span class=\"ft ft-plus\"></span>Add Tag</button>\r\n            <ngx-datatable #table class='bootstrap' [columns]=\"columns\" [columnMode]=\"'force'\" [headerHeight]=\"50\" [footerHeight]=\"50\"\r\n                [rowHeight]=\"'auto'\" [limit]=\"10\" [rows]='rows' [selected]=\"selected\" [selectionType]=\"'cell'\" (select)=\"onSelect($event)\">\r\n            </ngx-datatable>\r\n        </div>\r\n\r\n\r\n\r\n\r\n    </div>\r\n</section>\r\n<!-- Filter Datatable Options Ends -->\r\n\r\n\r\n\r\n        <div class=\"card-body\">\r\n            <div class=\"card-block\">\r\n                <ng-template #content let-c=\"close\" let-d=\"dismiss\">\r\n                    <div class=\"modal-header\">\r\n                        <h4 class=\"modal-title\">Add Tag</h4>\r\n                        <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"d('Cross click')\">\r\n                        <span aria-hidden=\"true\">&times;</span>\r\n                        </button>\r\n                    </div>\r\n                    <div class=\"modal-body\">\r\n\r\n\r\n                      \t<div class=\"row text-left justify-content-md-center\">\r\n                      \t\t<div class=\"\">\r\n                      \t\t\t<div class=\"\" style=\"width:400px\">\r\n                      \t\t\t\t<div class=\"card-body\">\r\n                      \t\t\t\t\t<div class=\"px-3\">\r\n                      \t\t\t\t\t\t<form class=\"form\">\r\n                      \t\t\t\t\t\t\t<div class=\"form-body\">\r\n\r\n                      \t\t\t\t\t\t\t\t<div class=\"form-group\">\r\n                      \t\t\t\t\t\t\t\t\t<label for=\"eventRegInput1\">Group Name</label>\r\n                      \t\t\t\t\t\t\t\t\t<input type=\"text\" id=\"eventRegInput1\" class=\"form-control\"  name=\"fullname\">\r\n                      \t\t\t\t\t\t\t\t</div>\r\n\r\n                      \t\t\t\t\t\t\t\t<div class=\"form-group\">\r\n                      \t\t\t\t\t\t\t\t\t<label for=\"eventRegInput2\">Group Description</label>\r\n                      \t\t\t\t\t\t\t\t\t<input type=\"text\" id=\"eventRegInput2\" class=\"form-control\"  name=\"title\">\r\n                      \t\t\t\t\t\t\t\t</div>\r\n\r\n                                      <div class=\"form-group\">\r\n                                        <label for=\"eventRegInput2\">Department</label>\r\n                                        <input type=\"text\" id=\"eventRegInput2\" class=\"form-control\"  name=\"title\">\r\n                                      </div>\r\n\r\n                                      <div class=\"form-group\">\r\n                                        <label for=\"eventRegInput2\">Policy</label>\r\n                                        <input type=\"text\" id=\"eventRegInput2\" class=\"form-control\"  name=\"title\">\r\n                                      </div>\r\n\r\n                      \t\t\t\t\t\t\t</div>\r\n\r\n                      \t\t\t\t\t\t\t<div class=\"form-actions center\">\r\n                      \t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-raised btn-warning mr-1\">\r\n                      \t\t\t\t\t\t\t\t\t<i class=\"ft-x\"></i> Cancel\r\n                      \t\t\t\t\t\t\t\t</button>\r\n                      \t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-raised btn-primary\">\r\n                      \t\t\t\t\t\t\t\t\t<i class=\"fa fa-check-square-o\"></i> Save\r\n                      \t\t\t\t\t\t\t\t</button>\r\n                      \t\t\t\t\t\t\t</div>\r\n                      \t\t\t\t\t\t</form>\r\n                      \t\t\t\t\t</div>\r\n                      \t\t\t\t</div>\r\n                      \t\t\t</div>\r\n                      \t\t</div>\r\n                      \t</div>\r\n\r\n\r\n                    </div>\r\n                    <div class=\"modal-footer\">\r\n                        <button type=\"button\" class=\"btn btn-secondary btn-raised\" (click)=\"c('Close click')\">Close</button>\r\n                    </div>\r\n                </ng-template>\r\n\r\n            </div>\r\n        </div>\r\n"

/***/ }),

/***/ "./src/app/player/player.component.scss":
/*!**********************************************!*\
  !*** ./src/app/player/player.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/player/player.component.ts":
/*!********************************************!*\
  !*** ./src/app/player/player.component.ts ***!
  \********************************************/
/*! exports provided: PlayerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PlayerComponent", function() { return PlayerComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _swimlane_ngx_datatable_release__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @swimlane/ngx-datatable/release */ "./node_modules/@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var _swimlane_ngx_datatable_release__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_swimlane_ngx_datatable_release__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var data = __webpack_require__(/*! ../shared/data/tags.json */ "./src/app/shared/data/tags.json");
var PlayerComponent = /** @class */ (function () {
    function PlayerComponent(router, route, modalService) {
        this.router = router;
        this.route = route;
        this.modalService = modalService;
        this.rows = [];
        this.selected = [];
        this.temp = [];
        // Table Column Titles
        this.columns = [
            { prop: 'Name' },
            { name: 'Policy' },
            { name: 'Department' },
            { name: 'Devicecount' },
            { name: 'Usercount' },
            { name: 'Description' },
        ];
        this.temp = data.slice();
        this.rows = data;
    }
    PlayerComponent.prototype.onSelect = function (event) {
        console.log("onslect");
        // this.router.navigate(['/taskboard'], { relativeTo: this.route.parent });
    };
    //  On Activation of dataTable's data row
    PlayerComponent.prototype.onActivate = function (event) {
        console.log("onaCTIVATE");
    };
    //   updateFilter(event) {
    //     console.log(event);
    //       const val = event.target.value.toLowerCase();
    //
    //       // filter our data
    //       const temp = this.temp.filter(function (d) {
    //           return d.name.toLowerCase().indexOf(val) !== -1 || !val;
    //       });
    //
    //       // update the rows
    //       this.rows = temp;
    //       // Whenever the filter changes, always go back to the first page
    //       this.table.offset = 0;
    //   // }
    // Open default modal
    PlayerComponent.prototype.open = function (content) {
        var _this = this;
        this.modalService.open(content).result.then(function (result) {
            _this.closeResult = "Closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
        });
    };
    // This function is used in open
    PlayerComponent.prototype.getDismissReason = function (reason) {
        if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["ModalDismissReasons"].ESC) {
            return 'by pressing ESC';
        }
        else if (reason === _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["ModalDismissReasons"].BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        }
        else {
            return "with: " + reason;
        }
    };
    // Open modal with dark section
    PlayerComponent.prototype.openModal = function (customContent) {
        this.modalService.open(customContent, { windowClass: 'dark-modal' });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_swimlane_ngx_datatable_release__WEBPACK_IMPORTED_MODULE_1__["DatatableComponent"]),
        __metadata("design:type", _swimlane_ngx_datatable_release__WEBPACK_IMPORTED_MODULE_1__["DatatableComponent"])
    ], PlayerComponent.prototype, "table", void 0);
    PlayerComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-player',
            template: __webpack_require__(/*! ./player.component.html */ "./src/app/player/player.component.html"),
            styles: [__webpack_require__(/*! ./player.component.scss */ "./src/app/player/player.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModal"]])
    ], PlayerComponent);
    return PlayerComponent;
}());



/***/ }),

/***/ "./src/app/player/player.module.ts":
/*!*****************************************!*\
  !*** ./src/app/player/player.module.ts ***!
  \*****************************************/
/*! exports provided: PlayerModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PlayerModule", function() { return PlayerModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var videogular2_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! videogular2/core */ "./node_modules/videogular2/core.js");
/* harmony import */ var videogular2_core__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(videogular2_core__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var videogular2_controls__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! videogular2/controls */ "./node_modules/videogular2/controls.js");
/* harmony import */ var videogular2_controls__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(videogular2_controls__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var videogular2_overlay_play__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! videogular2/overlay-play */ "./node_modules/videogular2/overlay-play.js");
/* harmony import */ var videogular2_overlay_play__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(videogular2_overlay_play__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var videogular2_buffering__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! videogular2/buffering */ "./node_modules/videogular2/buffering.js");
/* harmony import */ var videogular2_buffering__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(videogular2_buffering__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @swimlane/ngx-datatable */ "./node_modules/@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _player_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./player-routing.module */ "./src/app/player/player-routing.module.ts");
/* harmony import */ var _player_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./player.component */ "./src/app/player/player.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var PlayerModule = /** @class */ (function () {
    function PlayerModule() {
    }
    PlayerModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _player_routing_module__WEBPACK_IMPORTED_MODULE_7__["PlayerRoutingModule"],
                videogular2_core__WEBPACK_IMPORTED_MODULE_2__["VgCoreModule"],
                videogular2_controls__WEBPACK_IMPORTED_MODULE_3__["VgControlsModule"],
                videogular2_overlay_play__WEBPACK_IMPORTED_MODULE_4__["VgOverlayPlayModule"],
                videogular2_buffering__WEBPACK_IMPORTED_MODULE_5__["VgBufferingModule"],
                _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6__["NgxDatatableModule"]
            ],
            declarations: [
                _player_component__WEBPACK_IMPORTED_MODULE_8__["PlayerComponent"]
            ]
        })
    ], PlayerModule);
    return PlayerModule;
}());



/***/ }),

/***/ "./src/app/shared/data/tags.json":
/*!***************************************!*\
  !*** ./src/app/shared/data/tags.json ***!
  \***************************************/
/*! exports provided: 0, 1, default */
/***/ (function(module) {

module.exports = [{"Name":"QDM001","policy":"Default","department":"Sales","devicecount":"100","usercount":"100","description":"this group applies to sales dept"},{"Name":"QDM002","policy":"Default","department":"Operations","devicecount":"200","usercount":"200","description":"this group applies to operations dept"}];

/***/ })

}]);
//# sourceMappingURL=player-player-module.js.map