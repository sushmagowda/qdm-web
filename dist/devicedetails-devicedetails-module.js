(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["devicedetails-devicedetails-module"],{

/***/ "./src/app/devicedetails/devicedetails-routing.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/devicedetails/devicedetails-routing.module.ts ***!
  \***************************************************************/
/*! exports provided: DevicedetailsRoutingModule, routedComponents */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DevicedetailsRoutingModule", function() { return DevicedetailsRoutingModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routedComponents", function() { return routedComponents; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _devicedetails_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./devicedetails.component */ "./src/app/devicedetails/devicedetails.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '',
        component: _devicedetails_component__WEBPACK_IMPORTED_MODULE_2__["DevicedetailsComponent"],
        data: {
            title: 'DiviceList'
        },
    }
];
var DevicedetailsRoutingModule = /** @class */ (function () {
    function DevicedetailsRoutingModule() {
    }
    DevicedetailsRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
        })
    ], DevicedetailsRoutingModule);
    return DevicedetailsRoutingModule;
}());

var routedComponents = [_devicedetails_component__WEBPACK_IMPORTED_MODULE_2__["DevicedetailsComponent"]];


/***/ }),

/***/ "./src/app/devicedetails/devicedetails.component.html":
/*!************************************************************!*\
  !*** ./src/app/devicedetails/devicedetails.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  devicedetails works!\n</p>\n"

/***/ }),

/***/ "./src/app/devicedetails/devicedetails.component.scss":
/*!************************************************************!*\
  !*** ./src/app/devicedetails/devicedetails.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/devicedetails/devicedetails.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/devicedetails/devicedetails.component.ts ***!
  \**********************************************************/
/*! exports provided: DevicedetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DevicedetailsComponent", function() { return DevicedetailsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DevicedetailsComponent = /** @class */ (function () {
    function DevicedetailsComponent() {
    }
    DevicedetailsComponent.prototype.ngOnInit = function () {
    };
    DevicedetailsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-devicedetails',
            template: __webpack_require__(/*! ./devicedetails.component.html */ "./src/app/devicedetails/devicedetails.component.html"),
            styles: [__webpack_require__(/*! ./devicedetails.component.scss */ "./src/app/devicedetails/devicedetails.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], DevicedetailsComponent);
    return DevicedetailsComponent;
}());



/***/ }),

/***/ "./src/app/devicedetails/devicedetails.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/devicedetails/devicedetails.module.ts ***!
  \*******************************************************/
/*! exports provided: DevicedetailsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DevicedetailsModule", function() { return DevicedetailsModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _devicedetails_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./devicedetails-routing.module */ "./src/app/devicedetails/devicedetails-routing.module.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var ngx_quill__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-quill */ "./node_modules/ngx-quill/index.js");
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @swimlane/ngx-datatable */ "./node_modules/@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _devicedetails_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./devicedetails.component */ "./src/app/devicedetails/devicedetails.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var DevicedetailsModule = /** @class */ (function () {
    function DevicedetailsModule() {
    }
    DevicedetailsModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _devicedetails_routing_module__WEBPACK_IMPORTED_MODULE_2__["DevicedetailsRoutingModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_3__["NgbModule"],
                ngx_quill__WEBPACK_IMPORTED_MODULE_4__["QuillModule"],
                _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_5__["NgxDatatableModule"]
            ],
            declarations: [
                _devicedetails_component__WEBPACK_IMPORTED_MODULE_6__["DevicedetailsComponent"]
            ]
        })
    ], DevicedetailsModule);
    return DevicedetailsModule;
}());



/***/ })

}]);
//# sourceMappingURL=devicedetails-devicedetails-module.js.map