import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TagdetailsComponent } from './tagdetails.component';

const routes: Routes = [
  {
    path: '',
     component: TagdetailsComponent,
    data: {
      title: 'Tagdetails'
    },

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TagDetailsRoutingModule { }

export const routedComponents = [TagdetailsComponent];
