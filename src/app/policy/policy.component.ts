import { Component, ViewChild} from '@angular/core';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";
import { Router, ActivatedRoute } from "@angular/router";
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

declare var require: any;
const data: any = require('../shared/data/policy.json');


@Component({
  selector: 'app-policy',
  templateUrl: './policy.component.html',
  styleUrls: ['./policy.component.scss']
})
export class PolicyComponent  {
closeResult: string;
  rows = [];
selected: any[] = [];
  temp = [];

  // Table Column Titles
  columns = [
      { prop: 'policy' },
      { name: 'Groups' },
      { name: 'Devices' }
  ];
  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(private router: Router,
      private route: ActivatedRoute,private modalService: NgbModal ) {
      this.temp = [...data];
      this.rows = data;
  }
  onSelect(event) {
   console.log("onslect");
     this.router.navigate(['/taskboard-ngrx'], { relativeTo: this.route.parent });
  }

  //  On Activation of dataTable's data row
  onActivate(event) {
    console.log("onaCTIVATE");
  }


  // Open default modal
  open(content) {
      this.modalService.open(content).result.then((result) => {
          this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
  }

  // This function is used in open
  private getDismissReason(reason: any): string {
      if (reason === ModalDismissReasons.ESC) {
          return 'by pressing ESC';
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
          return 'by clicking on a backdrop';
      } else {
          return `with: ${reason}`;
      }
  }

  // Open modal with dark section
  openModal(customContent) {
      this.modalService.open(customContent, { windowClass: 'dark-modal' });
  }

  // Open content with dark section
  // openContent() {
  //     const modalRef = this.modalService.open(NgbdModalContent);
  //     modalRef.componentInstance.name = 'World';
  // }
}
