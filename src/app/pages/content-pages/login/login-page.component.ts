import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-login-page',
    templateUrl: './login-page.component.html',
    styleUrls: ['./login-page.component.scss']
})

export class LoginPageComponent {

    @ViewChild('f') loginForm: NgForm;

    constructor(private router: Router,
        private route: ActivatedRoute,public toastr: ToastrService) { }

    // On submit button click
    onSubmit() {
          console.log(this.loginForm.value);
          if(this.loginForm.value.inputEmail =='qualidm' && this.loginForm.value.inputPass == 'qualidm')
          {
            this.toastr.success('Logged in', 'Succssfully');
          this.router.navigate(['/dashboard/dashboard1'], { relativeTo: this.route.parent });
         }
         else
         {
          // this.loginForm.value.inputEmail = '';
           //this.loginForm.value.inputPass = '';
           this.toastr.success('Username and Password','Please Enter Valid ');

         }
    }
    // On Forgot password link click
    onForgotPassword() {
        this.router.navigate(['forgotpassword'], { relativeTo: this.route.parent });
    }
    // On registration link click
    onRegister() {
        this.router.navigate(['register'], { relativeTo: this.route.parent });
    }
}
